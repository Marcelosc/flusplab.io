---
title:  "GCC"
image:  "gcc"
ref: "ulxrandr"
categories: "projects.md"
id: 4
---

# Description

Cross compiler for C, C++, Fortran, Go, Obj-C, ...

# Recommendations

* Regex
* automata
* free-context grammars
* C
* a bit of C++ (basic knowledge of templates)
* Subversion
* Assembler
* Floating point

# URLs

* [GCC SVN View](https://gcc.gnu.org/viewcvs/gcc/)
