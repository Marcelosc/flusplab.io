---
title:  "Platinum"
image:  "platinum"
ref: "platinum"
categories: "patrocinio.md"
id: 2
---

# Valor: R$1500

# Benefícios

* Identificação da marca patrocinadora em todo material de divulgação do evento: online e offline.
* Logo no Crachá.
* Agradecimento e reconhecimento do patrocinador em todas as atividades relacionadas ao evento.
* Oportunidade de exibir banner do patrocinador no evento 
* Possibilidade de apresentação de até 15 minutos do patrocinador na abertura do evento
