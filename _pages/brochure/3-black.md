---
title:  "Black"
image:  "black"
ref: "black"
categories: "brochure.md"
id: 3
---

# Value: &gt;= R$2200

# Benefits

* Identification of the sponsoring brand in all event material: online and offline.
* Logo on the badge.
* Acknowledgment of the sponsor in all activities related to the event.
* Opportunity to display the sponsor's banner at the event;
* Opportunity to make a 15 minutes speech at the event opening.
* Company logo recognized for 1 year on the FLUSP sponsors page;
* Company logo included on videos published by FLUSP for 1 year.
