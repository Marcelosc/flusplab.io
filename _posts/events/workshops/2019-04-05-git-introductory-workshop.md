---
layout: post
categories: events
title: "Git Introductory Workshop"
lang: en
author: matheustavares
redirect_from: /events/2019/04/05/git-introductory-workshop
excerpt_separator: <!--end-abstract-->
---

As part of the IMEx series of events, FLUSP brought to the new IME students an
introductory workshop to the beauty of Git!

<!--end-abstract-->

[IMEx](https://www.facebook.com/events/730040700729918/) is a month of events
offered by IME-USP's extension groups to welcome the new students and present
them to the groups' activities. Last Monday, as our contribution to this
series of events, FLUSP hosted a three-part Git workshop, presenting what it
is and how to use it. The event went through:

1. A conceptual presentation on what is Git and why it's needed. You may find
   these slides (in PT-BR) [here](/materials/Git_Basico.pdf).
2. A running example of Git's commands being used to track a C project. Here we
   introduced the audience from repository creation up to branches, workflow and
   remotes.
3. A hands-on [game](https://github.com/Gazler/githug). We set up the machines
   and assisted the students over a very nice game that helps to understand and
   get used to Git commands.

Here you can see some of the event photos:

{% include slideshow.html
    slideShowIndex = 0
    images = "1.jpg,2.jpg,3.jpg,4.jpg,5.jpg"
    caption_list="1,2,3,4,5"
%}

We thank everyone who participated in the activity and hope to see you again
soon!
